package main.java;

import main.java.luhn_algorithm.LuhnAlgorithm;

import java.util.Arrays;
import java.util.Scanner;

public class Program {

    public static void main(String[] args) {
        LuhnAlgorithm luhnAlgorithm = new LuhnAlgorithm();
        Scanner scanner = new Scanner(System.in);
        System.out.println("Hi there!");
        System.out.println("Please fill in credit card number");
        System.out.print("Input: ");
        String userCard = scanner.nextLine().trim();
        int lastDigit = Integer.parseInt(userCard.split("")[userCard.split("").length - 1]);
        System.out.println("Input: " + userCard.substring(0, userCard.length() - 1) + " " + lastDigit);
        System.out.println("Provided: " + lastDigit);
        System.out.println("Expected: " + luhnAlgorithm.luhnDigit(userCard));
        System.out.println("");
        System.out.println("Checksum: " + luhnAlgorithm.ValidUserInput(luhnAlgorithm.isValidUserInput(userCard)));
        System.out.println("Digits: " + userCard.length() + " " + luhnAlgorithm.creditCard(userCard));
    }



}
