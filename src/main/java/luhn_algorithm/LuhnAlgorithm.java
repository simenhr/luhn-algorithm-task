package main.java.luhn_algorithm;

import java.util.*;
import java.util.stream.Collector;
import java.util.stream.Collectors;

public class LuhnAlgorithm {

    public LuhnAlgorithm() {

    }

    public boolean luhnCheck(String vat) {
        if (isValidUserInput(vat) == true) {
            int checkDigit = luhnDigit(vat);
            return (checkDigit == Integer.parseInt(vat.split("")[vat.split("").length - 1]));
        }
        return false;
    }

    public int luhnDigit(String vat) {
        if(isValidUserInput(vat) == true) {
            int value = 0;
            int sum = 0;
            int index = 0;
            List<Integer> newVat;
            String[] splitVat = vat.split("");
            String[] newSplitVat = Arrays.copyOf(splitVat, splitVat.length - 1);
            newVat = Arrays.asList(newSplitVat).stream().map(s -> Integer.parseInt(s)).collect(Collectors.toList());
            Collections.reverse(newVat);
            for (Integer elem : newVat) {
                value = elem;
                if (index % 2 == 0) {
                    value *= 2;
                    if (value > 9) {
                        value = 1 + (value % 10);
                    }
                }
                sum += value;
                index++;
            }
            return (sum * 9) % 10;
        }
        return -1;
    }

    public String creditCard(String vat) {
        if(isCreditCard(vat) == true) {
            return "(Credit Card)";
        }
        return "(Unknown type of card)";

    }

    public Boolean isCreditCard(String vat) {
        return(vat.length() == 16 && isValidUserInput(vat));

    }



    public Boolean isValidUserInput(String userInput) {
            return userInput.matches("[0-9]+");
    }

    public String ValidUserInput(Boolean isValidUserInput) {
        if(isValidUserInput) {
            return "Valid";
        }
        return "Invalid";
    }

}
