package main.java.functionality;

import main.java.luhn_algorithm.LuhnAlgorithm;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class LuhnAlgorithmTest {

    LuhnAlgorithm luhnAlgorithm = new LuhnAlgorithm();

    @Test
    void luhnCheckTest() {
        String test1 = "10101010105";
        String test2 = "10101010106";
        String test3 = "4242424242424242";
        assertTrue(luhnAlgorithm.luhnCheck(test1));
        assertFalse(luhnAlgorithm.luhnCheck(test2));
        assertTrue(luhnAlgorithm.luhnCheck(test3));
    }

    @Test
    void luhnDigitTest() {
        String test1 = "10101010105";
        String test2 = "10101010106";
        String test3 = "4242424242424242";
        assertEquals(5, luhnAlgorithm.luhnDigit(test1));
        assertEquals(5, luhnAlgorithm.luhnDigit(test2));
        assertEquals(2, luhnAlgorithm.luhnDigit(test3));
    }

    @Test
    void isCreditCardTest() {
        String test1 = "1234567890123456";
        String test2 = "21335235234";
        assertTrue(luhnAlgorithm.isCreditCard(test1));
        assertFalse(luhnAlgorithm.isCreditCard(test2));
    }

    @Test
    void creditCardTest() {
        String test1 = "1234567890123456";
        String test2 = "21335235234";
        assertEquals("(Credit Card)", luhnAlgorithm.creditCard(test1));
        assertEquals("(Unknown type of card)", luhnAlgorithm.creditCard(test2));
    }

    @Test
    void isValidUserInputTest() {
        String test1 = "12385782346578";
        String test2 = "3276t4jh83724";
        assertTrue(luhnAlgorithm.isValidUserInput(test1));
        assertFalse(luhnAlgorithm.isValidUserInput(test2));

    }




}